import { editCase } from "./sudoku-drawer";

const fullPossibilitiesList = [1, 2, 3, 4, 5, 6, 7, 8, 9];

export function runScan(sudoku: number[][]) {
    let startTime = performance.now();
    let emptySquaresCount = calculateEmptySquaresCount(sudoku);
    let performanceString = emptySquaresCount + " cases résolues en "
    // for (let i = 0; i < 1; i++) {
    while (emptySquaresCount > 0) {
        for (let x = 0; x < sudoku.length; x++) {
            for (let y = 0; y < sudoku[x].length; y++) {
                if (sudoku[x][y] === null) {
                    let possibilitiesList = [...fullPossibilitiesList];
                    possibilitiesList = checkForSquare(sudoku, possibilitiesList, x, y)
                    if (possibilitiesList.length === 1) {
                        sudoku[x][y] = possibilitiesList[0];
                        editCase(x, y, sudoku[x][y])
                        emptySquaresCount--;
                    }
                }
            }
        }
    }
    let endTime = performance.now();
    console.log(performanceString + (endTime - startTime) + "ms");
}

function checkForSquare(sudoku: number[][], possibilitiesList: number[], x: number, y: number) {
    possibilitiesList = checkLine(sudoku, possibilitiesList, x);
    possibilitiesList = checkColumn(sudoku, possibilitiesList, y);
    possibilitiesList = checkBigSquare(sudoku, possibilitiesList, x, y);
    return possibilitiesList
}

function checkLine(sudoku: number[][], possibilitiesList: number[], ligneX: number) {
    for (const num of sudoku[ligneX]) {
        possibilitiesList = removeIfNotInList(possibilitiesList, num)
    }
    return possibilitiesList
}

function checkColumn(sudoku: number[][], possibilitiesList: number[], columnY: number) {
    for (let x = 0; x < 9; x++) {
        possibilitiesList = removeIfNotInList(possibilitiesList, sudoku[x][columnY])
    }
    return possibilitiesList
}

function checkBigSquare(sudoku: number[][], possibilitiesList: number[], ligneX: number, columnY: number) {
    let bSquareX = ligneX - (ligneX % 3);
    let bSquareY = columnY - (columnY % 3);

    for (let checkX = bSquareX; checkX < bSquareX + 3; checkX++) {
        for (let checkY = bSquareY; checkY < bSquareY + 3; checkY++) {
            possibilitiesList = removeIfNotInList(possibilitiesList, sudoku[checkX][checkY])
        }
    }
    return possibilitiesList;
}

function removeIfNotInList(list: number[], value: number) {
    if (!isNaN(value) && list.indexOf(value) !== -1)
        list.splice(list.indexOf(value), 1);
    return list;
}

function calculateEmptySquaresCount(sudoku: number[][]): number {
    let num = 0
    for (let x = 0; x < sudoku.length; x++) {
        for (let y = 0; y < sudoku[x].length; y++) {
            if (sudoku[x][y] === null)
                num++;
        }
    }
    return num
}
