import { sudoku } from "./sudoku";
import { drawDifficulties } from "./sudoku-builder";
import { drawSudoku } from "./sudoku-drawer";
import { runScan } from "./sudoku-solver"
// import sudokus from "url:../public/sudokus.txt"
// import csvFileUrl from 'url:./file.csv'

init()

document.getElementById("solve-btn").addEventListener("click", (e) => {
    runScan(sudoku)
})

function init() {
    drawSudoku(sudoku)
    drawDifficulties();
}

