const table = document.querySelector("table");

export function drawSudoku(sudoku: number[][]) {
    for (let x = 0; x < sudoku.length; x++) {
        let row = table.insertRow();
        for (let y = 0; y < sudoku[x].length; y++) {
            let th = document.createElement("td");
            let text = sudoku[x][y] ? sudoku[x][y].toString() : "";
            th.innerText = text;
            th.id = getUniqueId(x, y)
            row.appendChild(th);
        }
    }
}

export function editCase(x: number, y: number, value: number) {
    let square = document.getElementById(getUniqueId(x, y))
    square.innerText = value.toString();
}

function getUniqueId(x: number, y: number): string {
    return "case-" + x + "-" + y;
}