const difficulties =
{
    "easy": 62,
    "medium": 53,
    "hard": 44,
    "very-hard": 35,
    "insane": 26,
    "inhuman": 17
}

export function drawDifficulties() {
    let difficultyDiv = document.getElementById("difficulties");
    for (const diff in difficulties) {
        let button = document.createElement("button");
        button.innerText = diff
        button.addEventListener("click", (e) => {
            generateSudoku(diff)
        });
        difficultyDiv.appendChild(button)
    }
}

function generateSudoku(difficulty: string) {
    console.log("IN TH NAVT");

    let sudoku: number[][] = [];
    let numberCount = 0;
    while (numberCount < difficulties[difficulty]) {
        for (let x = 0; x < 9; x++) {
            sudoku[x] = []
            for (let y = 0; y < 9; y++) {
                // Génère un nombre entre 1 et 81 => On remplit les cases de manière aléatoire.
                if (Math.floor(Math.random() * (81) + 1) === 1) {
                    // On remplit la case d'un nombre aléatoire entre 1 et 9.
                    sudoku[x][y] = Math.floor(Math.random() * (9) + 1);
                    numberCount++
                };
            }
        }
    }

    return sudoku
}